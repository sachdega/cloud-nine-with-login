package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String name ) {
	          Boolean isValid = false;

	            if(name.length() >= 6) {

	                if(Pattern.matches("^[a-zA-Z0-9]*$",  name)) {

	                    if(Character.isDigit(name.charAt(0))) {
	                        throw new NumberFormatException("Login Name can not start with a number");
	                    } 

	                    else {
	                        isValid = true;
	                    }
	            } 

	            else {
	              throw new NumberFormatException("Login Name should start with a number!");
	            }

	            }
	            else {
	                throw new NumberFormatException("Login username should be atleast6 or more chracters!");
	            }
	            return isValid;    }
	
}
