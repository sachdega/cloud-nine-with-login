package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;

public class LoginValidatorTest {
	

	    @Test
	    public void testIsValidNameRegular( ) {
	        assertTrue("Invalid Login" , LoginValidator.isValidLoginName("sachdeva"));
	    }

	    @Test(expected=NumberFormatException.class)
	    public void testIsValidNameException() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("garry"));
	    }

	    @Test
	    public void testIsValidNameBoundaryIn() {
	        assertTrue("Invalid Login", LoginValidator.isValidLoginName("gaura1"));
	    }
	    @Test(expected=NumberFormatException.class)
	    public void testIsValidNameBoundaryOut() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("gau29"));
	    }
	    
	    
	      

	    @Test
	    public void testIsValidCharactersBoundaryIn() {
	        assertTrue("Invalid Login", LoginValidator.isValidLoginName("gau123"));
	    }

	    @Test
	    public void testIsValidCharacterBoundaryIn() {
	        assertTrue("Invalid Login", LoginValidator.isValidLoginName("gaura1"));
	    }


	    @Test(expected=NumberFormatException.class)
	    public void testIsValidCharacterBoundaryOut() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("g@urav"));
	    }

	    @Test(expected=NumberFormatException.class)
	    public void testIsValidStartingCharacterBoundaryOut() {
	        assertFalse("Invalid Login", LoginValidator.isValidLoginName("2gaurav"));
	    }

	  
	}
